import { GamesappFrontendPage } from './app.po';

describe('gamesapp-frontend App', function() {
    let page: GamesappFrontendPage;

  beforeEach(() => {
      page = new GamesappFrontendPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
