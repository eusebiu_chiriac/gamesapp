export class GridDataRequest {
  PageNumber: number;
  PageSize: number;
  FilterValue: string;
  DateLow: Date;
  DateHigh: Date;
  SortField: string;
  SortOrder: number;
}
