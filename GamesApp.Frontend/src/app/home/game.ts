export class Game {
    categories: string[]
    description: string
    name: string
    newGame: boolean
    popularity: number
    shortName: string
    slug: string
    tags: string[]
    vendor: string
}
