﻿import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { GridDataRequest } from './GridDataRequest';
import { DialogModule, OverlayPanel } from 'primeng/primeng';
import { PageData } from './PageData';
import { Game } from './game';
import { OverlayPanelModule } from 'primeng/primeng';


@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    providers: []
})
export class HomeComponent implements OnInit {
    title = 'appfrontend';
    apiRoot: string = "http://localhost:8000/api";
    deLa: string;
    panaLa: string;
    name: string;
    selectedgame: Game = new Game();
    pageData: PageData = new PageData();
    gridDataRequest: GridDataRequest = new GridDataRequest();
    display: boolean = false;

    ngOnInit() {
        this.selectedgame = new Game();
    }

    constructor(private http: Http) { }

    getAllGames() {
        let url = `${this.apiRoot}` + '/GetAllGames';
        this.http.get(url).map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    this.pageData = data;
                },
                err => console.log(err), // error
                () => console.log('Get all games Complete') // complete
            );
    }

    paginate(event) {
        if (event.sortField != undefined) {
            var text = event.sortField;
            text = text.split('.')
                .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
                .join('.');
            this.gridDataRequest.SortField = text;
        }
        this.gridDataRequest.SortOrder = event.sortOrder;
        this.gridDataRequest.PageNumber = event.first / event.rows;
        this.gridDataRequest.PageSize = event.rows;
        this.gridDataRequest.FilterValue = this.name;

        let url = `${this.apiRoot}` + '/GetGames';
        this.http.post(url, { gridRequest: this.gridDataRequest }).map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    this.pageData = data;
                },
                err => console.log(err), // error
                () => console.log('Paginate Complete') // complete
            );
    }

    Search() {
        let url = `${this.apiRoot}` + '/GetByName';
        this.http.post(url, { name: this.name }).map((data: any) => data.json())
            .subscribe(
                (data: any) => {
                    this.pageData = data;
                },
                err => console.log(err), // error
                () => console.log('Search Complete') // complete
            );
    }

    showDialog() {
        this.display = true;
    }

    Reset() {
        this.getAllGames();
        this.name = "";
    }

    selectGame(event, game: Game, overlaypanel: OverlayPanel) {
        this.selectedgame = game;
        overlaypanel.toggle(event);
    }
}