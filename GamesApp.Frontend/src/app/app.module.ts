﻿import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from "@angular/router";
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { CarouselModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { SpinnerModule } from 'primeng/primeng';
import { ConfirmDialogModule, ConfirmationService } from 'primeng/primeng';
import { MessagesModule } from 'primeng/primeng';
import { GrowlModule } from 'primeng/primeng';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { PaginatorModule } from 'primeng/primeng';
import { ReactiveFormsModule } from '@angular/forms';
import { FieldsetModule } from 'primeng/primeng';
import { SliderModule } from 'primeng/primeng';
import { CalendarModule } from 'primeng/primeng';
import { AutoCompleteModule } from 'primeng/primeng';
import { DataTableModule, SharedModule } from 'primeng/primeng';
import { DialogModule } from 'primeng/primeng';
import { TooltipModule } from 'primeng/primeng';
import { PanelModule } from 'primeng/primeng';
import { DropdownModule } from 'primeng/primeng';
import { OverlayPanelModule } from 'primeng/primeng';
import { FileUploadModule } from 'primeng/primeng';



const appRoutes: Routes = [
    { path: "", redirectTo: "home", pathMatch: "full" },
    { path: "home", component: HomeComponent }
];

@NgModule({
  declarations: [
      AppComponent,
      HomeComponent
  ],
  imports: [
      BrowserModule,
      CarouselModule,
      FieldsetModule,
      ConfirmDialogModule,
      OverlayPanelModule,
    FormsModule,
    RouterModule.forRoot(appRoutes),
      HttpModule,
      ButtonModule,
      SpinnerModule,
      ConfirmDialogModule,
      MessagesModule,
      GrowlModule,
      BrowserAnimationsModule,
      PaginatorModule,
      SliderModule,
      CalendarModule,
      AutoCompleteModule,
      DataTableModule,
      SharedModule,
      DialogModule,
      TooltipModule,
      ReactiveFormsModule,
      PanelModule,
      DropdownModule,
      FileUploadModule
      
      
  ],
  providers: [ConfirmationService],
  bootstrap: [AppComponent]
})
export class AppModule { }
