const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
var _ = require("underscore");
const fs = require('fs-extra');

const app = express();

const PORT = 8000;

app.use(bodyParser.json());
app.use(cors());

app.get('/', (req, res) => {
    res.send("<h1>salut</h1>");
});

app.post('/api/GetByName', (req, res) => {
    var bodyRequest = req.body;
    var games = originalGames;
    var ret = games.filter(game => game.name.toLowerCase().startsWith(bodyRequest.name.toLowerCase()));
    var o = { TotalJocuri: ret.length, Data: ret};
    res.json(o);  
});

function dynamicSort(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a, b) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

app.get('/api/GetAllGames', (req, res) => {
    var games = originalGames;
    var o = { TotalJocuri: games.length, Data: games };
    res.json(o);
});

const originalGames = JSON.parse(fs.readFileSync('./games.json').toString());

app.post('/api/GetGames', (req, res) => {
    var bodyRequest = req.body;
    var games = [...originalGames];
    var ret;
    if (bodyRequest && bodyRequest.gridRequest && bodyRequest.gridRequest.FilterValue) {
        games = games.filter(game => game.name.toLowerCase().startsWith(bodyRequest.gridRequest.FilterValue.toLowerCase()));
    }
    if (bodyRequest && bodyRequest.gridRequest && bodyRequest.gridRequest.SortField) {
        bodyRequest.gridRequest.SortField = bodyRequest.gridRequest.SortField.toLowerCase();

        if (bodyRequest.gridRequest.SortOrder === -1)
            ret = games.sort(dynamicSort('-' + bodyRequest.gridRequest.SortField));
        else 
            ret = games.sort(dynamicSort(bodyRequest.gridRequest.SortField));
    }
    else {
        ret = games;
    }

    var o = { TotalJocuri: ret.length, Data: ret.splice(bodyRequest.gridRequest.PageNumber * bodyRequest.gridRequest.PageSize, bodyRequest.gridRequest.PageSize) };
    res.json(o);
});

app.listen(PORT, () => {
    console.log(`Server is listening on port ${PORT}.`);
});